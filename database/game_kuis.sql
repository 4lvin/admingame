-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 20, 2019 at 06:26 PM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `game_kuis`
--

DELIMITER $$
--
-- Functions
--
CREATE DEFINER=`root`@`localhost` FUNCTION `rownum` () RETURNS INT(11) BEGIN
  set @prvrownum=if(@ranklastrun=CURTIME(6),@prvrownum+1,1);
  set @ranklastrun=CURTIME(6);
  RETURN @prvrownum;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `data_setting`
--

CREATE TABLE `data_setting` (
  `id_setting` int(11) NOT NULL,
  `key_setting` varchar(50) DEFAULT NULL,
  `nama_setting` varchar(255) DEFAULT NULL,
  `value_setting` varchar(255) DEFAULT NULL,
  `keterangan` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_setting`
--

INSERT INTO `data_setting` (`id_setting`, `key_setting`, `nama_setting`, `value_setting`, `keterangan`) VALUES
(1, 'setting_rumus', 'tampil', '1', 'settingan rumus dimunculkan atau tidak di pertanyaan'),
(2, 'setting_rumus', 'tidak tampil', '0', 'settingan rumus dimunculkan atau tidak di pertanyaan'),
(3, 'aktif_soal', 'aktif', '1', 'settingan pertanyaan apakah aktif atau tidak'),
(4, 'aktif_soal', 'tidak aktif', '0', 'settingan pertanyaan apakah aktif atau tidak');

-- --------------------------------------------------------

--
-- Table structure for table `m_jenis_bangunan`
--

CREATE TABLE `m_jenis_bangunan` (
  `id_jenis_bangunan` int(11) NOT NULL,
  `nama_jenis_bangunan` varchar(50) DEFAULT NULL,
  `file_name` text,
  `url` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_jenis_bangunan`
--

INSERT INTO `m_jenis_bangunan` (`id_jenis_bangunan`, `nama_jenis_bangunan`, `file_name`, `url`) VALUES
(1, 'PERSEGI', 'PERSEGI_20190525100829_persegi.jpg', 'http://localhost/admingame/uploads/PERSEGI_20190525100829_persegi.jpg'),
(2, 'PERSEGI PANJANG', 'PANJANG_20190525101403_persegi_panjang.png', 'http://localhost/admingame/uploads/PERSEGI PANJANG_20190525101403_persegi_panjang.png'),
(3, 'JAJAR GENJANG', 'JAJAR_GENJANG_20190525101712_jajar_genjang.png', 'http://localhost/admingame/uploads/JAJAR GENJANG_20190525101712_jajar_genjang.png'),
(4, 'SEGITIGA', 'SEGITIGA_20190525103224_segitiga.png', 'http://localhost/admingame/uploads/SEGITIGA_20190525103224_segitiga.png'),
(5, 'BELAH KETUPAT', 'BELAH_KETUPAT_20190525103206_Belah_Ketupat.png', 'http://localhost/admingame/uploads/BELAH_KETUPAT_20190525103206_Belah_Ketupat.png'),
(17, 'tessa', 'tessa_20190525045145_srgl_band.JPG', 'http://localhost/admingame/uploads/tessa_20190525045145_srgl_band.JPG'),
(19, 'a', 'a_20190525045724_acce_dms_security.png', 'http://localhost/admingame/uploads/a_20190525045724_acce_dms_security.png');

-- --------------------------------------------------------

--
-- Table structure for table `m_jenis_rumus`
--

CREATE TABLE `m_jenis_rumus` (
  `id_jenis_rumus` int(11) NOT NULL,
  `nama_jenis_rumus` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_jenis_rumus`
--

INSERT INTO `m_jenis_rumus` (`id_jenis_rumus`, `nama_jenis_rumus`) VALUES
(1, 'LUAS'),
(2, 'KELILING'),
(5, 'fadfk'),
(6, 'luasss');

-- --------------------------------------------------------

--
-- Table structure for table `m_level`
--

CREATE TABLE `m_level` (
  `id_level` int(11) NOT NULL,
  `nama_level` varchar(50) DEFAULT NULL,
  `show_rumus` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_level`
--

INSERT INTO `m_level` (`id_level`, `nama_level`, `show_rumus`) VALUES
(1, 'easy', '1'),
(2, 'medium', '0'),
(3, 'hard', '0');

-- --------------------------------------------------------

--
-- Table structure for table `m_pertanyaan`
--

CREATE TABLE `m_pertanyaan` (
  `id_pertanyaan` int(11) NOT NULL,
  `judul` varchar(255) DEFAULT NULL,
  `pertanyaan` text,
  `pilihan_a` varchar(50) DEFAULT NULL,
  `pilihan_b` varchar(50) DEFAULT NULL,
  `pilihan_c` varchar(50) DEFAULT NULL,
  `pilihan_d` varchar(50) DEFAULT NULL,
  `kunci_jawaban` varchar(50) DEFAULT NULL,
  `id_level` int(11) DEFAULT NULL,
  `id_jenis_bangunan` int(11) DEFAULT NULL,
  `id_jenis_rumus` int(11) DEFAULT NULL,
  `id_rumus` int(11) DEFAULT NULL,
  `is_aktif` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_pertanyaan`
--

INSERT INTO `m_pertanyaan` (`id_pertanyaan`, `judul`, `pertanyaan`, `pilihan_a`, `pilihan_b`, `pilihan_c`, `pilihan_d`, `kunci_jawaban`, `id_level`, `id_jenis_bangunan`, `id_jenis_rumus`, `id_rumus`, `is_aktif`) VALUES
(1, 'PERSEGI', 'sebuah persegi dengan panjang sisi 5 cm. Berapakah luas bangun tersebut?', '10cm', '20cm', '15cm', '25cm', '25cm', 1, 1, 1, 1, 1),
(2, 'PERSEGI PANJANG', 'Terdapat persegi panjang dengan panjang 20cm, serta lebar 10cm, berapa luasnya?', '30cm', '210cm', '120cm', '200cm', '200cm', 1, 2, 1, 3, 1),
(3, 'JAJAR GENJANG', 'Bangunan tersebut memiliki panjang alas 12cm dengan tinggi 5cm. Berapakah luas bangunannya?', '17cm', '8cm', '60cm', '52cm', '60cm', 1, 3, 1, 5, 1),
(4, 'SEGITIGA', 'Sebuah bangunan segitiga dengan panjang alas 7cm dan memiliki tinggi 5cm. Berapakah luasnya?', '35cm', '12cm', '17,5cm', '12,5cm', '17,5cm', 3, 4, 1, 7, 1),
(5, 'PERSEGI', 'Terdapat sebuah persegi dengan panjang sisi 2cm, berapakah keliling bangunan tersebut?', '4cm', '2cm', '8cm', '5cm', '8cm', 3, 1, 1, 1, 1),
(6, 'PERSEGI PANJANG', 'Sebuah persegi panjang dengan panjang 3cm dan lebar 7,5cm, berapakah kelilingnya?', '11,5cm', '20cm', '5,5cm', '21cm', '21cm', 2, 2, 2, 4, 1),
(7, 'BELAH KETUPAT', 'Sebuah belah ketupat dengan sisi 3cm, berapakah kelilingnya?', '9cm', '6cm', '12cm', '15cm', '12cm', 2, 5, 2, 10, 1),
(8, 'test', 'test', 'test', 'test', 'test', 'test', 'test', 2, 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `m_rumus`
--

CREATE TABLE `m_rumus` (
  `id_rumus` int(11) NOT NULL,
  `id_jenis_bangunan` int(11) DEFAULT NULL,
  `id_jenis_rumus` int(11) DEFAULT NULL,
  `rumus` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_rumus`
--

INSERT INTO `m_rumus` (`id_rumus`, `id_jenis_bangunan`, `id_jenis_rumus`, `rumus`) VALUES
(1, 1, 1, 'S × S'),
(2, 1, 2, '4 × S'),
(3, 2, 1, 'P × S'),
(4, 2, 2, '2 × (p + l))'),
(5, 3, 1, 'A × T'),
(6, 3, 2, '2 × (a + b)'),
(7, 4, 1, '1/2 × A × T'),
(8, 4, 2, 'sisi1 + sisi2 + sisi3'),
(9, 5, 1, '1/2 × d1 × d2'),
(10, 5, 2, 's + s + s + s'),
(11, 1, 5, 'test'),
(12, 1, 6, 'd');

-- --------------------------------------------------------

--
-- Table structure for table `m_user`
--

CREATE TABLE `m_user` (
  `id_user` int(11) NOT NULL,
  `nama_user` varchar(50) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `password` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_user`
--

INSERT INTO `m_user` (`id_user`, `nama_user`, `username`, `password`) VALUES
(1, 'admin', 'admin', '21232f297a57a5a743894a0e4a801fc3');

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_pertanyaan_aktif`
-- (See below for the actual view)
--
CREATE TABLE `v_pertanyaan_aktif` (
`nomer` int(11)
,`id_pertanyaan` int(11)
,`judul` varchar(255)
,`pertanyaan` text
,`pilihan_a` varchar(50)
,`pilihan_b` varchar(50)
,`pilihan_c` varchar(50)
,`pilihan_d` varchar(50)
,`kunci_jawaban` varchar(50)
,`id_level` int(11)
,`nama_level` varchar(50)
,`show_rumus` varchar(255)
,`nama_setting` varchar(255)
,`nama_jenis_bangunan` varchar(50)
,`nama_jenis_rumus` varchar(50)
,`rumus` varchar(50)
,`url` text
,`file_name` text
);

-- --------------------------------------------------------

--
-- Structure for view `v_pertanyaan_aktif`
--
DROP TABLE IF EXISTS `v_pertanyaan_aktif`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_pertanyaan_aktif`  AS  select `rownum`() AS `nomer`,`a`.`id_pertanyaan` AS `id_pertanyaan`,`a`.`judul` AS `judul`,`a`.`pertanyaan` AS `pertanyaan`,`a`.`pilihan_a` AS `pilihan_a`,`a`.`pilihan_b` AS `pilihan_b`,`a`.`pilihan_c` AS `pilihan_c`,`a`.`pilihan_d` AS `pilihan_d`,`a`.`kunci_jawaban` AS `kunci_jawaban`,`a`.`id_level` AS `id_level`,`b`.`nama_level` AS `nama_level`,`b`.`show_rumus` AS `show_rumus`,`c`.`nama_setting` AS `nama_setting`,`d`.`nama_jenis_bangunan` AS `nama_jenis_bangunan`,`e`.`nama_jenis_rumus` AS `nama_jenis_rumus`,`f`.`rumus` AS `rumus`,`d`.`url` AS `url`,`d`.`file_name` AS `file_name` from (((((`m_pertanyaan` `a` left join `m_level` `b` on((`a`.`id_level` = `b`.`id_level`))) left join `data_setting` `c` on(((`a`.`is_aktif` = `c`.`value_setting`) and (`c`.`key_setting` = 'aktif_soal')))) left join `m_jenis_bangunan` `d` on((`a`.`id_jenis_bangunan` = `d`.`id_jenis_bangunan`))) left join `m_jenis_rumus` `e` on((`a`.`id_jenis_rumus` = `e`.`id_jenis_rumus`))) left join `m_rumus` `f` on((`a`.`id_rumus` = `f`.`id_rumus`))) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `data_setting`
--
ALTER TABLE `data_setting`
  ADD PRIMARY KEY (`id_setting`);

--
-- Indexes for table `m_jenis_bangunan`
--
ALTER TABLE `m_jenis_bangunan`
  ADD PRIMARY KEY (`id_jenis_bangunan`);

--
-- Indexes for table `m_jenis_rumus`
--
ALTER TABLE `m_jenis_rumus`
  ADD PRIMARY KEY (`id_jenis_rumus`);

--
-- Indexes for table `m_level`
--
ALTER TABLE `m_level`
  ADD PRIMARY KEY (`id_level`);

--
-- Indexes for table `m_pertanyaan`
--
ALTER TABLE `m_pertanyaan`
  ADD PRIMARY KEY (`id_pertanyaan`);

--
-- Indexes for table `m_rumus`
--
ALTER TABLE `m_rumus`
  ADD PRIMARY KEY (`id_rumus`);

--
-- Indexes for table `m_user`
--
ALTER TABLE `m_user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `data_setting`
--
ALTER TABLE `data_setting`
  MODIFY `id_setting` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `m_jenis_bangunan`
--
ALTER TABLE `m_jenis_bangunan`
  MODIFY `id_jenis_bangunan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `m_jenis_rumus`
--
ALTER TABLE `m_jenis_rumus`
  MODIFY `id_jenis_rumus` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `m_level`
--
ALTER TABLE `m_level`
  MODIFY `id_level` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `m_pertanyaan`
--
ALTER TABLE `m_pertanyaan`
  MODIFY `id_pertanyaan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `m_rumus`
--
ALTER TABLE `m_rumus`
  MODIFY `id_rumus` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `m_user`
--
ALTER TABLE `m_user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
