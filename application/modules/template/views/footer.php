     <!-- Page Footer-->
     <footer class="main-footer">
       <div class="container-fluid">
         <div class="row">
           <div class="col-sm-6">
             <p>Prod &copy; 2019</p>
           </div>
           <div class="col-sm-6 text-right">
             <p>Design by Mr G &copy; 2019</p>
           </div>
         </div>
       </div>
     </footer>
     </div>
     </div>
     </div>

     <div class="modal fade" id="logOutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
       <div class="modal-dialog" role="document">
         <div class="modal-content">
           <div class="modal-header">
             <h5 class="modal-title" id="exampleModalLabel">Keluar</h5>
           </div>
           <div class="modal-body">Anda Yakin Untuk Keluar?</div>
           <div class="modal-footer">
             <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
             <a class="btn btn-danger" href="<?php echo site_url('login/logout'); ?>"><i class="fas fa-sign-out-alt"></i>
               Keluar</a>
           </div>
         </div>
       </div>
     </div>
     </body>