        <div class="page-content d-flex align-items-stretch">
          <!-- Side Navbar -->
          <nav class="side-navbar">
            <!-- Sidebar Header-->
            <div class="sidebar-header d-flex align-items-center">
              <!-- <div class="avatar"> -->
              <!-- <img src="<?php echo base_url('asset/tema/admin/img/logomasjid.jpeg" alt="..." class="img-fluid rounded-circle '); ?>"> -->
              <!-- </div> -->
              <div class="title">
                <h1 class="h4">ADMIN</h1>
              </div>
            </div>
            <!-- Sidebar Navidation Menus-->
            <span class="heading">Main</span>
            <ul class="list-unstyled">
              <li class="active"><a href="<?php echo base_url('dashboard/dashboard'); ?>"> <i class="icon-home"></i>Home </a></li>
              <li><a href="#masterDropdown" aria-expanded="false" data-toggle="collapse"> <i class="icon-interface-windows"></i>Master </a>
                <ul id="masterDropdown" class="collapse list-unstyled ">
                  <li><a href="<?php echo base_url('master/pertanyaan'); ?>">Master Pertanyaan</a></li>
                  <li><a href="<?php echo base_url('master/level'); ?>">Master Level</a></li>
                  <li><a href="<?php echo base_url('master/jenis_bangunan'); ?>">Master Jenis Bangunan</a></li>
                  <li><a href="<?php echo base_url('master/jenis_rumus'); ?>">Master Jenis Rumus</a></li>
                  <li><a href="<?php echo base_url('master/rumus'); ?>">Master Rumus</a></li>
                </ul>
              </li>
              <li><a href="<?php echo base_url('master/user'); ?>"> <i class="icon-user"></i>User </a></li>
            </ul>
          </nav>`