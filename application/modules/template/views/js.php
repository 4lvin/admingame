<!-- Javascript files-->
<script src="<?php echo base_url('assets/tema/admin/js/fontawesome-all.js'); ?>"></script>
<script src="<?php echo base_url('assets/tema/admin/js/jquery-3.2.1.js'); ?>"></script>
<script src="<?php echo base_url('assets/tema/admin/vendor/popper.js/umd/popper.min.js'); ?>"> </script>
<script src="<?php echo base_url('assets/tema/admin/vendor/bootstrap/js/bootstrap.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/tema/admin/vendor/jquery.cookie/jquery.cookie.js'); ?>"> </script>
<script src="<?php echo base_url('assets/tema/admin/vendor/chart.js/Chart.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/tema/admin/vendor/jquery-validation/jquery.validate.min.js'); ?>"></script>
<!-- Main File-->
<script src="<?php echo base_url('assets/tema/admin/js/front.js'); ?>"></script>
<script src="<?php echo base_url('assets/tema/admin/js/jquery.dataTables.js'); ?>"></script>
<script src="<?php echo base_url('assets/tema/admin/js/dataTables.bootstrap4.js'); ?>"></script>
<script src="<?php echo base_url('assets/lib/sweetalert2/sweetalert.js'); ?>"></script>
<script src="<?php echo base_url('assets/lib/block-ui/jquery.blockUI.js'); ?>"></script>
