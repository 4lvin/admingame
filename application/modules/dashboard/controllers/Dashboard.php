<?php 
if (!defined("BASEPATH"))
    exit("No direct script access allowed");

class Dashboard extends MX_Controller{

	function __construct(){
		parent::__construct();		
		$this->load->helper('url');	
		$this->load->model('login/Login_model', 'admin');
	}

	function index(){

		if($this->admin->logged_id())
        {

			$this->load->view('template/header');
			$this->load->view('template/js');
			$this->load->view('template/navbar');
			$this->load->view('template/sidebar');
			$this->load->view('dashboard');
			$this->load->view('template/footer');        

        }else{

            redirect("login");

        }
    }
}
?>