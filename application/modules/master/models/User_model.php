<?php

class User_model extends CI_Model
{

  function get_user()
  {
    $hasil = $this->db->query("SELECT * FROM m_user ");
    return $hasil->result();
  }

  function get_user_by($id)
  {
    $hasil = $this->db->query("SELECT * FROM m_user where id_user ='" . $id . "'");
    return $hasil->result();
  }

  function insert_user($data)
  {
    return $this->db->insert('m_user', $data);
  }

  function update_user($data, $id)
  {
    $this->db->where('id_user', $id);

    return $this->db->update('m_user', $data);
  }

  function delete_user($id)
  {
    $this->db->where_in('id_user', $id);
    return   $this->db->delete('m_user');
  }

  function check_nama_user($nama_user)
  {
    $query = $this->db->get_where('m_user', array('nama_user' => $nama_user));

    if ($query->num_rows() > 0) {
        return FALSE;
      } else {
        return TRUE;
      }
  }
}
