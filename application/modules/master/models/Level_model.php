<?php

class Level_model extends CI_Model
{

  function get_level()
  {
    $hasil = $this->db->query("SELECT a.id_level, a.nama_level, b.nama_setting FROM m_level a
    LEFT OUTER JOIN data_setting b ON a.show_rumus =b.value_setting AND b.key_setting = 'setting_rumus' order by a.id_level");
    return $hasil->result();
  }

  function get_level_by($id)
  {
    $hasil = $this->db->query("SELECT * FROM m_level where id_level ='" . $id . "'");
    return $hasil->result();
  }

  function insert_level($data)
  {
    return $this->db->insert('m_level', $data);
  }

  function update_level($data, $id)
  {
    $this->db->where('id_level', $id);

    return $this->db->update('m_level', $data);
  }

  function delete_level($id)
  {
    $this->db->where_in('id_level', $id);
    return   $this->db->delete('m_level');
  }

  function check_nama_level($nama_level)
  {
    $query = $this->db->get_where('m_level', array('nama_level' => $nama_level));

    if ($query->num_rows() > 0) {
        return FALSE;
      } else {
        return TRUE;
      }
  }
}
