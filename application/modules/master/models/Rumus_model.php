<?php

class Rumus_model extends CI_Model
{

	function get_rumus()
	{
		$hasil = $this->db->query("SELECT a.id_rumus, b.nama_jenis_bangunan, c.nama_jenis_rumus, a.rumus FROM 
		m_rumus a
		LEFT OUTER JOIN m_jenis_bangunan b ON a.id_jenis_bangunan = b.id_jenis_bangunan
		LEFT OUTER JOIN m_jenis_rumus c ON a.id_jenis_rumus =c.id_jenis_rumus");
		return $hasil->result();
	}

	function get_rumus_by($id)
	{
		$hasil = $this->db->query("SELECT a.id_rumus, a.id_jenis_bangunan, b.nama_jenis_bangunan, a.id_jenis_rumus, 
			c.nama_jenis_rumus, a.rumus FROM m_rumus a
			LEFT OUTER JOIN m_jenis_bangunan b ON a.id_jenis_bangunan = b.id_jenis_bangunan
			LEFT OUTER JOIN m_jenis_rumus c ON a.id_jenis_rumus =c.id_jenis_rumus
			where a.id_rumus ='" . $id . "'");
		return $hasil->result();
	}

	function insert_rumus($data)
	{
		return $this->db->insert('m_rumus', $data);
	}

	function update_rumus($data, $id)
	{
		$this->db->where('id_rumus', $id);

		return $this->db->update('m_rumus', $data);
	}

	function delete_rumus($id)
	{
		$this->db->where_in('id_rumus', $id);
		return   $this->db->delete('m_rumus');
	}

	function check_rumus($id_jenis_bangunan, $id_jenis_rumus)
	{
	  $query = $this->db->get_where('m_rumus', array('id_jenis_bangunan' => $id_jenis_bangunan, 'id_jenis_rumus' => $id_jenis_rumus));
	//   var_dump($query);
  
	  if ($query->num_rows() > 0) {
		  return FALSE;
		} else {
		  return TRUE;
		}
	}
}
