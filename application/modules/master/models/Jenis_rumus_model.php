<?php

class Jenis_rumus_model extends CI_Model
{

  function get_jenis_rumus()
  {
    $hasil = $this->db->query("SELECT * FROM m_jenis_rumus ");
    return $hasil->result();
  }

  function get_jenis_rumus_by($id)
  {
    $hasil = $this->db->query("SELECT * FROM m_jenis_rumus where id_jenis_rumus ='" . $id . "'");
    return $hasil->result();
  }

  function insert_jenis_rumus($data)
  {
    return $this->db->insert('m_jenis_rumus', $data);
  }

  function update_jenis_rumus($data, $id)
  {
    $this->db->where('id_jenis_rumus', $id);

    return $this->db->update('m_jenis_rumus', $data);
  }

  function delete_jenis_rumus($id)
  {
    $this->db->where_in('id_jenis_rumus', $id);
    return   $this->db->delete('m_jenis_rumus');
  }

  function check_nama_jenis_rumus($nama_jenis_rumus)
  {
    $query = $this->db->get_where('m_jenis_rumus', array('nama_jenis_rumus' => $nama_jenis_rumus));

    if ($query->num_rows() > 0) {
        return FALSE;
      } else {
        return TRUE;
      }
  }
}
