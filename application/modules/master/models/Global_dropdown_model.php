<?php

class Global_dropdown_model extends CI_Model
{
    // get data setting untuk pilihan rumus apapakah ditampilkan di aplikasi android
    function get_show_rumus()
    {
        $query = $this->db->query("SELECT nama_setting, value_setting FROM data_setting WHERE key_setting = 'setting_rumus'");
        return $query->result();
    }

    // get data setting untuk status aktif soal di pertanyaan
    function get_status_soal(){
        $query = $this->db->query("SELECT nama_setting, value_setting FROM data_setting WHERE key_setting = 'aktif_soal'");
        return $query->result();
    }
}
