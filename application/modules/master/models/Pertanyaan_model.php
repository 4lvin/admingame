<?php

class Pertanyaan_model extends CI_Model
{

	function get_pertanyaan()
	{
		$hasil = $this->db->query("SELECT a.id_pertanyaan, a.judul, a.pertanyaan, a.pilihan_a, a.pilihan_b, 
		a.pilihan_c, a.pilihan_d, a.kunci_jawaban, a.pertanyaan, b.nama_level, c.nama_setting, d.nama_jenis_bangunan,
		e.nama_jenis_rumus, f.rumus  FROM m_pertanyaan a
		left outer join m_level b on a.id_level = b.id_level
		LEFT OUTER JOIN data_setting c ON a.is_aktif = c.value_setting AND c.key_setting ='aktif_soal'
		LEFT OUTER JOIN m_jenis_bangunan d ON a.id_jenis_bangunan = d.id_jenis_bangunan
		LEFT OUTER JOIN m_jenis_rumus e ON a.id_jenis_rumus = e.id_jenis_rumus
		LEFT OUTER JOIN m_rumus f ON a.id_rumus = f.id_rumus order by a.id_pertanyaan");
		return $hasil->result();
	}

	function get_pertanyaan_aktif()
	{
		$hasil = $this->db->query("SELECT * FROM v_pertanyaan_aktif");
		return $hasil->result();
	}

	function get_pertanyaan_aktif_by($id_level, $nomer)
	{
		$hasil = $this->db->query("SELECT * FROM v_pertanyaan_aktif where id_level ='".$id_level."' and nomer ='".$nomer."'");
		return $hasil->result();
	}

	function get_pertanyaan_by($id)
	{
		$hasil = $this->db->query("SELECT a.id_pertanyaan, a.judul, a.pertanyaan, a.pilihan_a, a.pilihan_b, 
		a.pilihan_c, a.pilihan_d, a.kunci_jawaban, a.id_pertanyaan, a.pertanyaan, a.is_aktif, a.id_level, b.nama_level, c.nama_setting, a.id_jenis_bangunan, d.nama_jenis_bangunan,
		a.id_rumus, e.nama_jenis_rumus, f.rumus  FROM m_pertanyaan a
		left outer join m_level b on a.id_level = b.id_level
		LEFT OUTER JOIN data_setting c ON a.is_aktif = c.value_setting AND c.key_setting ='aktif_soal'
		LEFT OUTER JOIN m_jenis_bangunan d ON a.id_jenis_bangunan = d.id_jenis_bangunan
		LEFT OUTER JOIN m_jenis_rumus e ON a.id_jenis_rumus = e.id_jenis_rumus
		LEFT OUTER JOIN m_rumus f ON a.id_rumus = f.id_rumus where a.id_pertanyaan ='" . $id . "'");
		return $hasil->result();
	}

	function get_rumus_by($id_jenis_bangunan, $id_jenis_rumus)
	{
		$hasil = $this->db->query("SELECT id_rumus FROM m_rumus WHERE id_jenis_bangunan ='" . $id_jenis_bangunan . "' AND id_jenis_rumus = '" . $id_jenis_rumus . "'");
		return $hasil->row();
	}

	function insert_pertanyaan($data)
	{
		return $this->db->insert('m_pertanyaan', $data);
	}

	function update_pertanyaan($data, $id)
	{
		$this->db->where('id_pertanyaan', $id);

		return $this->db->update('m_pertanyaan', $data);
	}

	function delete_pertanyaan($id)
	{
		$this->db->where_in('id_pertanyaan', $id);
		return   $this->db->delete('m_pertanyaan');
	}
}
