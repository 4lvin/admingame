<?php

class Jenis_bangunan_model extends CI_Model
{

  function get_jenis_bangunan()
  {
    $hasil = $this->db->query("SELECT * FROM m_jenis_bangunan order by id_jenis_bangunan");
    return $hasil->result();
  }

  function get_jenis_bangunan_by($id)
  {
    $hasil = $this->db->query("SELECT * FROM m_jenis_bangunan where id_jenis_bangunan ='" . $id . "'");
    return $hasil->result();
  }

  function insert_jenis_bangunan($data)
  {
    return $this->db->insert('m_jenis_bangunan', $data);
  }

  function update_jenis_bangunan($data, $id)
  {
    $this->db->where('id_jenis_bangunan', $id);

    return $this->db->update('m_jenis_bangunan', $data);
  }

  function delete_jenis_bangunan($id)
  {
    $this->db->where_in('id_jenis_bangunan', $id);
    return   $this->db->delete('m_jenis_bangunan');
  }

  function check_nama_jenis_bangunan($nama_jenis_bangunan)
  {
    $query = $this->db->get_where('m_jenis_bangunan', array('nama_jenis_bangunan' => $nama_jenis_bangunan));

    if ($query->num_rows() > 0) {
      return FALSE;
    } else {
      return TRUE;
    }
  }
}
