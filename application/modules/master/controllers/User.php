
<?php
if (!defined("BASEPATH"))
	exit("No direct script access allowed");

class user extends MX_Controller
{

	private $_title = 'Master User';

	function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('User_model', 'tbl_get');
		$this->load->model('login/Login_model', 'admin');
	}

	function index()
	{
		if ($this->admin->logged_id()) {
			$data['page_title'] = '<i class="icon-interface-windows"></i> ' . $this->_title;
			$data['data'] = $this->tbl_get->get_user();
			$this->load->view('template/header');
			$this->load->view('template/js');
			$this->load->view('template/navbar');
			$this->load->view('template/sidebar');
			$this->load->view('user/main', $data);
			$this->load->view('template/footer');
		} else {

			redirect("login");
		}
	}

	function form($id = '')
	{
		$page_title = 'Tambah User';
		if ($id != '') {
			$page_title = 'Edit User';
			$data['data'] = $this->tbl_get->get_user_by($id);
		}
		$data['page_title'] = '<i class="icon-interface-windows"></i> ' . $page_title;
		$this->load->view('template/header');
		$this->load->view('template/js');
		$this->load->view('template/navbar');
		$this->load->view('template/sidebar');
		$this->load->view('user/form', $data);
		$this->load->view('template/footer');
	}

	function proses()
	{
		$id = $this->input->post('id_user');
		$nama_user = $this->input->post('nama_user');
		$username = $this->input->post('username');
		$password_lama = $this->input->post('password_lama');
		$password_baru = $this->input->post('password_baru');
		$data = array(
			'nama_user' => $nama_user,
			'username' => $username,
			'password' => MD5($password_baru)
		);

		$get_user = $this->tbl_get->get_user_by($id);
		foreach ($get_user as $row)
			$password = $row->password;

		if ($password == $password_lama) {
			$this->edit($data, $id);
		} else {
			$d['status'] = 0;
			$d['pesan'] = "GAGAL! password lama tidak sesuai";

			echo json_encode($d);
		}
	}

	function edit($data, $id)
	{
		if ($this->tbl_get->update_user($data, $id)) {
			$d['status'] = 1;
			$d['pesan'] = "data berhasil disimpan";
		} else {
			$d['status'] = 0;
			$d['pesan'] = "GAGAL! data tidak berhasil disimpan";
		}
		echo json_encode($d);
	}
}
?>