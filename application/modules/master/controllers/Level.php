<?php
if (!defined("BASEPATH"))
	exit("No direct script access allowed");

class Level extends MX_Controller
{
	private $_title = 'Master Level';

	function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('Level_model', 'tbl_get');
		$this->load->model('Global_dropdown_model', 'get_global_dropdown');
		$this->load->model('login/Login_model', 'admin');
	}

	function index()
	{
		if ($this->admin->logged_id()) {
			$data['page_title'] = '<i class="icon-interface-windows"></i> ' . $this->_title;
			$data['data'] = $this->tbl_get->get_level();
			$this->load->view('template/header');
			$this->load->view('template/js');
			$this->load->view('template/navbar');
			$this->load->view('template/sidebar');
			$this->load->view('level/main', $data);
			$this->load->view('template/footer');
		} else {

			redirect("login");
		}
	}

	function form($id = '')
	{
		$page_title = 'Tambah Level';
		if ($id != '') {
			$page_title = 'Edit Level';
			$data['data'] = $this->tbl_get->get_level_by($id);
		}
		$data['page_title'] = '<i class="icon-interface-windows"></i> ' . $page_title;
		$data['option_show_rumus'] = $this->get_global_dropdown->get_show_rumus();
		$this->load->view('template/header');
		$this->load->view('template/js');
		$this->load->view('template/navbar');
		$this->load->view('template/sidebar');
		$this->load->view('level/form', $data);
		$this->load->view('template/footer');
	}

	function proses()
	{
		$id = $this->input->post('id_level');
		$nama_level = $this->input->post('nama_level');
		$show_rumus = $this->input->post('show_rumus');
		$data = array(
			'nama_level' => $nama_level,
			'show_rumus' => $show_rumus
		);

		if ($id == '') {
			if ($this->tbl_get->check_nama_level($nama_level) == FALSE) {
				$d['status'] = 0;
				$d['pesan'] = "GAGAL! level dengan nama '" . $nama_level . "' sudah ada";

				echo json_encode($d);
			} else {
				$this->tambah($data);
			}
		} else {
			$get_level = $this->tbl_get->get_level_by($id);
			foreach ($get_level as $row)
				$level = $row->nama_level;
			if ($nama_level == $level) {
				$this->edit($data, $id);
			} else {
				if ($this->tbl_get->check_nama_level($nama_level) == FALSE) {
					$d['status'] = 0;
					$d['pesan'] = "GAGAL! level dengan nama '" . $nama_level . "' sudah ada";

					echo json_encode($d);
				} else {
					$this->edit($data, $id);
				}
			}
		}
	}

	function tambah($data)
	{
		if ($this->tbl_get->insert_level($data)) {
			$d['status'] = 1;
			$d['pesan'] = "data berhasil disimpan";
		} else {
			$d['status'] = 0;
			$d['pesan'] = "GAGAL! data tidak berhasil disimpan";
		}
		echo json_encode($d);
	}

	function edit($data, $id)
	{
		if ($this->tbl_get->update_level($data, $id)) {
			$d['status'] = 1;
			$d['pesan'] = "data berhasil disimpan";
		} else {
			$d['status'] = 0;
			$d['pesan'] = "GAGAL! data tidak berhasil disimpan";
		}
		echo json_encode($d);
	}

	function hapus()
	{
		$id = $this->input->post('id_level');
		if ($id != "") {
			if ($this->tbl_get->delete_level($id)) {
				$d['status'] = 1;
				$d['pesan'] = "data berhasil dihapus.";
			} else {
				$d['status'] = 0;
				$d['pesan'] = "GAGAL! data tidak berhasil di hapus";
			}
		}
		echo json_encode($d);
	}
}
