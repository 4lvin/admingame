<?php
if (!defined("BASEPATH"))
    exit("No direct script access allowed");

require APPPATH . '/libraries/API_Controller.php';
class Android_api extends API_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('level_model', 'level_get');
        $this->load->model('Pertanyaan_model', 'pertanyaan_get');

    }

    function get_level()
    {
        header("Access-Control-Allow-Origin: *");
        
        $this->_apiConfig([
            'methods' => ['GET'],
        ]);

        $data = $this->level_get->get_level();

        if (!empty($data)) {
            $message = array('message' => "data tersedia");
        } else { 
            $message = array('message' => "data tidak tersedia");
        }

        $this->api_return(
            [
                'data' => $data,
                'message' => $message
            ],
            200
        );
    }

    function get_pertanyaan()
    {
        header("Access-Control-Allow-Origin: *");
        
        $this->_apiConfig([
            'methods' => ['GET'],
        ]);

        $data = $this->pertanyaan_get->get_pertanyaan_aktif();

        if (!empty($data)) {
            $message = array('message' => "data tersedia");
        } else { 
            $message = array('message' => "data tidak tersedia");
        }

        $this->api_return(
            [
                'data' => $data,
                'message' => $message
            ],
            200
        );
    }

    function get_pertanyaan_by()
    {
        header("Access-Control-Allow-Origin: *");

        $this->_apiConfig([
            'methods' => ['GET'],
        ]);

        $id_level = $this->input->get('id_level');
		$nomer = $this->input->get('nomer');

        $data = $this->pertanyaan_get->get_pertanyaan_aktif_by($id_level, $nomer);

        if (!empty($data)) {
            $message = array('message' => "data tersedia");
        } else { 
            $message = array('message' => "data tidak tersedia");
        }

        $this->api_return(
            [
                'data' => $data,
                'message' => $message
            ],
            200
        );
    }
}
