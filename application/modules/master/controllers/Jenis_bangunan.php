
<?php
if (!defined("BASEPATH"))
	exit("No direct script access allowed");

class Jenis_bangunan extends MX_Controller
{

	private $_title = 'Master Jenis Bangunan';

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Jenis_bangunan_model', 'tbl_get');
		$this->load->model('login/Login_model', 'admin');
	}

	public function index()
	{
		if ($this->admin->logged_id()) {

			$data['page_title'] = '<i class="icon-interface-windows"></i> ' . $this->_title;
			$data['data'] = $this->tbl_get->get_jenis_bangunan();
			$this->load->view('template/header');
			$this->load->view('template/js');
			$this->load->view('template/navbar');
			$this->load->view('template/sidebar');
			$this->load->view('jenis_bangunan/main', $data);
			$this->load->view('template/footer');
		} else {

			redirect("login");
		}
	}

	public function form($id = '')
	{
		$page_title = 'Tambah Jenis Bangunan';
		if ($id != '') {
			$page_title = 'Edit Jenis Bangunan';
			$data['data'] = $this->tbl_get->get_jenis_bangunan_by($id);
		}
		$data['page_title'] = '<i class="icon-interface-windows"></i> ' . $page_title;
		$this->load->view('template/header');
		$this->load->view('template/js');
		$this->load->view('template/navbar');
		$this->load->view('template/sidebar');
		$this->load->view('jenis_bangunan/form', $data);
		$this->load->view('template/footer');
	}


	function proses()
	{
		$id = $this->input->post('id_jenis_bangunan');
		$nama_jenis_bangunan = $this->input->post('nama_jenis_bangunan');
		if($nama_jenis_bangunan != ""){
			$foto_asli = $_FILES["filefoto"]['name'];
			$foto_remove_space = str_replace(" ", "_", $foto_asli);
			$foto_remove_left = str_replace("(", "", $foto_remove_space);
			$last_foto = str_replace(")", "", $foto_remove_left);
	
			$bangunan_remove_space = str_replace(" ", "_", $nama_jenis_bangunan);
			$bangunan_remove_left = str_replace("(", "", $bangunan_remove_space);
			$last_bangunan = str_replace(")", "", $bangunan_remove_left);
	
	
			$file_name = $last_bangunan . '_' . date("Ymdhis") . '_' . $last_foto;
			$url =  base_url() . "uploads/" . $file_name;
	
			if ($foto_asli != "") {
				$data = array(
					'nama_jenis_bangunan' => $nama_jenis_bangunan,
					'file_name' => $file_name,
					'url' => $url
				);
			} else {
				$data = array(
					'nama_jenis_bangunan' => $nama_jenis_bangunan
				);
			}
	
			$config =  array(
				'upload_path'     => dirname($_SERVER["SCRIPT_FILENAME"]) . "/uploads/",
				'upload_url'      => base_url() . "uploads/",
				'allowed_types'   => "jpg|png|jpeg",
				'overwrite'       => TRUE,
				'max_size'        => "1000KB",
				// ,
				// 'max_height'      => "768",
				// 'max_width'       => "1024"  
				'file_name'    => $file_name
			);
	
			$this->load->library('upload', $config);
	
			if ($id == '') {
				if ($this->tbl_get->check_nama_jenis_bangunan($nama_jenis_bangunan) == FALSE) {
					$d['status'] = 0;
					$d['pesan'] = "GAGAL! jenis bangunan '" . $nama_jenis_bangunan . "' sudah ada";
	
					echo json_encode($d);
				} else {
					if ($foto_asli != "") {
						if (!$this->upload->do_upload('filefoto')) {
							$d['status'] = 0;
							$d['pesan'] = "GAGAL! upload gambar gagal";
	
							echo json_encode($d);
						} else {
							$this->tambah($data);
						}
					} else {
						$this->tambah($data);
					}
				}
			} else {
				$get_jenis_bangunan = $this->tbl_get->get_jenis_bangunan_by($id);
				foreach ($get_jenis_bangunan as $row)
					$jenis_bangunan = $row->nama_jenis_bangunan;
					$file_name_db = $row->file_name;
				if ($nama_jenis_bangunan == $jenis_bangunan) {
					if($foto_asli != ""){
						if (!$this->upload->do_upload('filefoto')) {
							$d['status'] = 0;
							$d['pesan'] = "GAGAL! upload gambar gagal";
		
							echo json_encode($d);
						} else {
							if ($file_name_db != "") {
								unlink('uploads/' . $file_name_db);
							}
							$this->edit($data, $id);
						}
					}else{
						$this->edit($data, $id);
					}
				} else {
					if ($this->tbl_get->check_nama_jenis_bangunan($nama_jenis_bangunan) == FALSE) {
						$d['status'] = 0;
						$d['pesan'] = "GAGAL! jenis bangunan '" . $nama_jenis_bangunan . "' sudah ada";
	
						echo json_encode($d);
					} else {
						if($foto_asli != ""){
							if (!$this->upload->do_upload('filefoto')) {
								$d['status'] = 0;
								$d['pesan'] = "GAGAL! upload gambar gagal";
			
								echo json_encode($d);
							} else {
								if ($file_name_db != "") {
									unlink('uploads/' . $file_name_db);
								}
								$this->edit($data, $id);
							}
						}else{
							$this->edit($data, $id);
						}
					}
				}
			}
		}else{
			$d['status'] = 0;
			$d['pesan'] = "GAGAL! nama jenis bangunan harus diisi";

			echo json_encode($d);
		}
	}

	function tambah($data)
	{
		if ($this->tbl_get->insert_jenis_bangunan($data)) {
			$d['status'] = 1;
			$d['pesan'] = "data berhasil disimpan";
		} else {
			$d['status'] = 0;
			$d['pesan'] = "GAGAL! data tidak berhasil disimpan";
		}
		echo json_encode($d);
	}

	function edit($data, $id)
	{
		if ($this->tbl_get->update_jenis_bangunan($data, $id)) {
			$d['status'] = 1;
			$d['pesan'] = "data berhasil disimpan";
		} else {
			$d['status'] = 0;
			$d['pesan'] = "GAGAL! data tidak berhasil disimpan";
		}
		echo json_encode($d);
	}

	function hapus()
	{
		$id = $this->input->post('id_jenis_bangunan');
		$get_jenis_bangunan = $this->tbl_get->get_jenis_bangunan_by($id);
		foreach ($get_jenis_bangunan as $row)
			$file_name_db = $row->file_name;
			if($file_name_db != ""){
				unlink('uploads/' . $file_name_db);
			}
		
		if ($id != "") {
			if ($this->tbl_get->delete_jenis_bangunan($id)) {
				$d['status'] = 1;
				$d['pesan'] = "data berhasil dihapus.";
			} else {
				$d['status'] = 0;
				$d['pesan'] = "GAGAL! data tidak berhasil di hapus";
			}
		}
		echo json_encode($d);
	}
}
?>