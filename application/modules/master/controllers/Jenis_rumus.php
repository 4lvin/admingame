
<?php
if (!defined("BASEPATH"))
	exit("No direct script access allowed");

class Jenis_rumus extends MX_Controller
{

	private $_title = 'Master Jenis Rumus';

	function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('Jenis_rumus_model', 'tbl_get');
		$this->load->model('login/Login_model', 'admin');
	}

	function index()
	{
		if ($this->admin->logged_id()) {
			$data['page_title'] = '<i class="icon-interface-windows"></i> ' . $this->_title;
			$data['data'] = $this->tbl_get->get_jenis_rumus();
			$this->load->view('template/header');
			$this->load->view('template/js');
			$this->load->view('template/navbar');
			$this->load->view('template/sidebar');
			$this->load->view('jenis_rumus/main', $data);
			$this->load->view('template/footer');
		} else {

			redirect("login");
		}
	}

	function form($id = '')
	{
		$page_title = 'Tambah Jenis Rumus';
		if ($id != '') {
			$page_title = 'Edit Jenis Rumus';
			$data['data'] = $this->tbl_get->get_jenis_rumus_by($id);
		}
		$data['page_title'] = '<i class="icon-interface-windows"></i> ' . $page_title;
		$this->load->view('template/header');
		$this->load->view('template/js');
		$this->load->view('template/navbar');
		$this->load->view('template/sidebar');
		$this->load->view('jenis_rumus/form', $data);
		$this->load->view('template/footer');
	}

	function proses()
	{
		$id = $this->input->post('id_jenis_rumus');
		$nama_jenis_rumus = $this->input->post('nama_jenis_rumus');
		$data = array(
			'nama_jenis_rumus' => $nama_jenis_rumus
		);

		if ($id == '') {
			if ($this->tbl_get->check_nama_jenis_rumus($nama_jenis_rumus) == FALSE) {
				$d['status'] = 0;
				$d['pesan'] = "GAGAL! jenis rumus '" . $nama_jenis_rumus . "' sudah ada";

				echo json_encode($d);
			} else {
				$this->tambah($data);
			}
		} else {
			$get_jenis_rumus = $this->tbl_get->get_jenis_rumus_by($id);
			foreach ($get_jenis_rumus as $row)
				$jenis_rumus = $row->nama_jenis_rumus;
			if ($nama_jenis_rumus == $jenis_rumus) {
				$this->edit($data, $id);
			} else {
				if ($this->tbl_get->check_nama_jenis_rumus($nama_jenis_rumus) == FALSE) {
					$d['status'] = 0;
					$d['pesan'] = "GAGAL! jenis rumus '" . $nama_jenis_rumus . "' sudah ada";

					echo json_encode($d);
				} else {
					$this->edit($data, $id);
				}
			}
		}
	}

	function tambah($data)
	{
		if ($this->tbl_get->insert_jenis_rumus($data)) {
			$d['status'] = 1;
			$d['pesan'] = "data berhasil disimpan";
		} else {
			$d['status'] = 0;
			$d['pesan'] = "GAGAL! data tidak berhasil disimpan";
		}
		echo json_encode($d);
	}

	function edit($data, $id)
	{
		if ($this->tbl_get->update_jenis_rumus($data, $id)) {
			$d['status'] = 1;
			$d['pesan'] = "data berhasil disimpan";
		} else {
			$d['status'] = 0;
			$d['pesan'] = "GAGAL! data tidak berhasil disimpan";
		}
		echo json_encode($d);
	}

	function hapus()
	{
		$id = $this->input->post('id_jenis_rumus');
		if ($id != "") {
			if ($this->tbl_get->delete_jenis_rumus($id)) {
				$d['status'] = 1;
				$d['pesan'] = "data berhasil dihapus";
			} else {
				$d['status'] = 0;
				$d['pesan'] = "GAGAL! data tidak berhasil di hapus";
			}
		}
		echo json_encode($d);
	}
}
?>