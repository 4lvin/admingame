<?php
if (!defined("BASEPATH"))
	exit("No direct script access allowed");

class Pertanyaan extends MX_Controller
{
	private $_title = 'Master Pertanyaan';

	function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('Pertanyaan_model', 'tbl_get');
		$this->load->model('Jenis_bangunan_model', 'jenis_bangunan');
		$this->load->model('Jenis_rumus_model', 'jenis_rumus');
		$this->load->model('Level_model', 'level');
		$this->load->model('Global_dropdown_model', 'get_global_dropdown');
		$this->load->model('login/Login_model', 'admin');
	}

	function index()
	{
		if ($this->admin->logged_id()) {
			$data['page_title'] = '<i class="icon-interface-windows"></i> ' . $this->_title;
			$data['data'] = $this->tbl_get->get_pertanyaan_aktif();
			$this->load->view('template/header');
			$this->load->view('template/js');
			$this->load->view('template/navbar');
			$this->load->view('template/sidebar');
			$this->load->view('pertanyaan/main', $data);
			$this->load->view('template/footer');
		} else {

			redirect("login");
		}
	}

	function form($id = '')
	{
		$page_title = 'Tambah Pertanyaan';
		if ($id != '') {
			$page_title = 'Edit Pertanyaan';
			$data['data'] = $this->tbl_get->get_pertanyaan_by($id);
		}
		$data['page_title'] = '<i class="icon-interface-windows"></i> ' . $page_title;
		$data['option_jenis_bangunan'] = $this->jenis_bangunan->get_jenis_bangunan();
		$data['option_jenis_rumus'] = $this->jenis_rumus->get_jenis_rumus();
		$data['option_level'] = $this->level->get_level();
		$data['option_status_soal'] = $this->get_global_dropdown->get_status_soal();
		$this->load->view('template/header');
		$this->load->view('template/js');
		$this->load->view('template/navbar');
		$this->load->view('template/sidebar');
		$this->load->view('pertanyaan/form', $data);
		$this->load->view('template/footer');
	}

	function proses()
	{
		$id = $this->input->post('id_pertanyaan');
		$judul = $this->input->post('judul');
		$pertanyaan = $this->input->post('pertanyaan');
		$pilihan_a = $this->input->post('pilihan_a');
		$pilihan_b = $this->input->post('pilihan_b');
		$pilihan_c = $this->input->post('pilihan_c');
		$pilihan_d = $this->input->post('pilihan_d');
		$kunci_jawaban = $this->input->post('kunci_jawaban');
		$id_jenis_bangunan = $this->input->post('id_jenis_bangunan');
		$id_jenis_rumus = $this->input->post('id_jenis_rumus');
		$id_level = $this->input->post('id_level');
		$is_aktif = $this->input->post('is_aktif');
		$get_rumus = $this->tbl_get->get_rumus_by($id_jenis_bangunan, $id_jenis_rumus);
		$id_rumus =  $get_rumus->id_rumus;

		$data = array(
			'judul' => $judul,
			'pertanyaan' => $pertanyaan,
			'pilihan_a' => $pilihan_a,
			'pilihan_b' => $pilihan_b,
			'pilihan_c' => $pilihan_c,
			'pilihan_d' => $pilihan_d,
			'kunci_jawaban' => $kunci_jawaban,
			'id_jenis_bangunan' => $id_jenis_bangunan,
			'id_jenis_rumus' => $id_jenis_rumus,
			'id_level' => $id_level,
			'is_aktif' => $is_aktif,
			'id_rumus' => $id_rumus

		);

		if ($id == '') {
			$this->tambah($data);
		} else {
			$this->edit($data, $id);
		}
	}

	function tambah($data)
	{
		if ($this->tbl_get->insert_pertanyaan($data)) {
			$d['status'] = 1;
			$d['pesan'] = "data berhasil disimpan";
		} else {
			$d['status'] = 0;
			$d['pesan'] = "GAGAL! data tidak berhasil disimpan";
		}
		echo json_encode($d);
	}

	function edit($data, $id)
	{
		if ($this->tbl_get->update_pertanyaan($data, $id)) {
			$d['status'] = 1;
			$d['pesan'] = "data berhasil disimpan";
		} else {
			$d['status'] = 0;
			$d['pesan'] = "GAGAL! data tidak berhasil disimpan";
		}
		echo json_encode($d);
	}

	function hapus()
	{
		$id = $this->input->post('id_pertanyaan');
		if ($id != "") {
			if ($this->tbl_get->delete_pertanyaan($id)) {
				$d['status'] = 1;
				$d['pesan'] = "data berhasil dihapus.";
			} else {
				$d['status'] = 0;
				$d['pesan'] = "GAGAL! data tidak berhasil di hapus";
			}
		}
		echo json_encode($d);
	}
}
