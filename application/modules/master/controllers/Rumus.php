<?php
if (!defined("BASEPATH"))
	exit("No direct script access allowed");

class Rumus extends MX_Controller
{
	private $_title = 'Master Rumus';

	function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('Rumus_model', 'tbl_get');
		$this->load->model('Jenis_bangunan_model', 'jenis_bangunan');
		$this->load->model('Jenis_rumus_model', 'jenis_rumus');
		$this->load->model('login/Login_model', 'admin');
	}

	function index()
	{
		if ($this->admin->logged_id()) {
			$data['page_title'] = '<i class="icon-interface-windows"></i> ' . $this->_title;
			$data['data'] = $this->tbl_get->get_rumus();
			$this->load->view('template/header');
			$this->load->view('template/js');
			$this->load->view('template/navbar');
			$this->load->view('template/sidebar');
			$this->load->view('rumus/main', $data);
			$this->load->view('template/footer');
		} else {

			redirect("login");
		}
	}

	function form($id = '')
	{
		$page_title = 'Tambah Rumus';
		if ($id != '') {
			$page_title = 'Edit Rumus';
			$data['data'] = $this->tbl_get->get_rumus_by($id);
		}
		$data['page_title'] = '<i class="icon-interface-windows"></i> ' . $page_title;
		$data['option_jenis_bangunan'] = $this->jenis_bangunan->get_jenis_bangunan();
		$data['option_jenis_rumus'] = $this->jenis_rumus->get_jenis_rumus();
		$this->load->view('template/header');
		$this->load->view('template/js');
		$this->load->view('template/navbar');
		$this->load->view('template/sidebar');
		$this->load->view('rumus/form', $data);
		$this->load->view('template/footer');
	}

	function proses()
	{
		$id = $this->input->post('id_rumus');
		$id_jenis_bangunan = $this->input->post('id_jenis_bangunan');
		$id_jenis_rumus = $this->input->post('id_jenis_rumus');
		$rumus = $this->input->post('rumus');
		$data = array(
			'id_jenis_bangunan' => $id_jenis_bangunan,
			'id_jenis_rumus' => $id_jenis_rumus,
			'rumus' => $rumus
		);

		if ($id == '') {
			if ($this->tbl_get->check_rumus($id_jenis_bangunan, $id_jenis_rumus) == FALSE) {
				$d['status'] = 0;
				$d['pesan'] = "GAGAL! rumus dengan jenis bangunan dan jenis rumus tersebut sudah ada";

				echo json_encode($d);
			} else {
				$this->tambah($data);
			}
		} else {
			$get_rumus = $this->tbl_get->get_rumus_by($id);
			foreach ($get_rumus as $row)
				$jenis_bangunan = $row->id_jenis_bangunan;
			$jenis_rumus = $row->id_jenis_rumus;
			if ($id_jenis_bangunan == $jenis_bangunan && $id_jenis_rumus == $jenis_rumus) {
				$this->edit($data, $id);
			} else {
				if ($this->tbl_get->check_rumus($id_jenis_bangunan, $id_jenis_rumus) == FALSE) {
					$d['status'] = 0;
					$d['pesan'] = "GAGAL! rumus dengan jenis bangunan dan jenis rumus tersebut sudah ada";

					echo json_encode($d);
				} else {
					$this->edit($data, $id);
				}
			}
		}
	}

	function tambah($data)
	{
		if ($this->tbl_get->insert_rumus($data)) {
			$d['status'] = 1;
			$d['pesan'] = "data berhasil disimpan";
		} else {
			$d['status'] = 0;
			$d['pesan'] = "GAGAL! data tidak berhasil disimpan";
		}
		echo json_encode($d);
	}

	function edit($data, $id)
	{
		if ($this->tbl_get->update_rumus($data, $id)) {
			$d['status'] = 1;
			$d['pesan'] = "data berhasil disimpan";
		} else {
			$d['status'] = 0;
			$d['pesan'] = "GAGAL! data tidak berhasil disimpan";
		}
		echo json_encode($d);
	}

	function hapus()
	{
		$id = $this->input->post('id_rumus');
		if ($id != "") {
			if ($this->tbl_get->delete_rumus($id)) {
				$d['status'] = 1;
				$d['pesan'] = "data berhasil dihapus.";
			} else {
				$d['status'] = 0;
				$d['pesan'] = "GAGAL! data tidak berhasil di hapus";
			}
		}
		echo json_encode($d);
	}
}
