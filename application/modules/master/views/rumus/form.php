<div class="content-inner">
  <div class="col-lg-12">
    <div class="card">
      <div class="card-header d-flex align-items-center">
        <h3 class="h4"><?php echo isset($page_title) ? $page_title : 'Untitle'; ?></h3>
      </div>
      <div class="card-body">
        <form class="form-horizontal" action="<?php echo site_url('master/rumus/proses'); ?>" method="POST" id="form_rumus">
          <?php if (!empty($data))
            foreach ($data as $row)
              ?>
          <input name="id_rumus" type="hidden" value="<?php if (!empty($data)) echo $row->id_rumus ?>">
          <div class="form-group row">
            <label class="col-sm-3 form-control-label">Jenis Bangunan<sup> *</sup></label>
            <div class="col-sm-9">
              <SELECT class="form-control show-tick" name="id_jenis_bangunan">
                <?php foreach ($option_jenis_bangunan as $option_bangunan_jenis) {
                  if ($row->id_jenis_bangunan != $option_bangunan_jenis->id_jenis_bangunan) {
                    ?>
                    <option value="<?php echo $option_bangunan_jenis->id_jenis_bangunan; ?>"><?php echo $option_bangunan_jenis->nama_jenis_bangunan; ?></option>
                  <?php } else {
                  ?>
                    <option selected="selected" value="<?php echo $option_bangunan_jenis->id_jenis_bangunan; ?>"><?php echo $option_bangunan_jenis->nama_jenis_bangunan; ?></option>
                  <?php }
              } ?>
              </SELECT>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-sm-3 form-control-label">Jenis Rumus<sup> *</sup></label>
            <div class="col-sm-9">
              <SELECT class="form-control show-tick" name="id_jenis_rumus">
                <?php foreach ($option_jenis_rumus as $option_rumus_jenis) {
                  if ($row->id_jenis_rumus != $option_rumus_jenis->id_jenis_rumus) {
                    ?>
                    <option value="<?php echo $option_rumus_jenis->id_jenis_rumus; ?>"><?php echo $option_rumus_jenis->nama_jenis_rumus; ?></option>
                  <?php } else {
                  ?>
                    <option selected="selected" value="<?php echo $option_rumus_jenis->id_jenis_rumus; ?>"><?php echo $option_rumus_jenis->nama_jenis_rumus; ?></option>
                  <?php }
              } ?>
              </SELECT>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-sm-3 form-control-label">Rumus</label>
            <div class="col-sm-9">
              <input name="rumus" type="text" placeholder="Rumus" class="form-control form-control-success" value="<?php if (!empty($data)) echo $row->rumus ?>">
            </div>
          </div>
          <div class="form-group row">
            <div class="col-sm-9 offset-sm-3">
              <button type="submit" value="simpan" class="btn btn-primary" data-toggle="tooltip" title="Simpan">Simpan</button>
              <a href="<?php echo site_url('master/rumus'); ?>" class="btn btn-secondary" data-toggle="tooltip" title="kembali">Kembali</i></a>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>

  <script>
    $(document).ready(function() {

      $("#form_rumus").validate({
        // Specify validation rules
        rules: {
          // The key name on the left side is the name attribute
          id_jenis_bangunan: "required",
          id_jenis_rumus: "required",
          rumus: "required"

        },
        // Specify validation error messages
        messages: {
          id_jenis_bangunan: "pilihan jenis bangunan harus diisi",
          id_jenis_rumus: "pilihan jenis rumus harus diisi",
          rumus: "rumus harus diisi"
        },
        // Make sure the form is submitted to the destination defined
        // in the "action" attribute of the form when valid
        submitHandler: function(form) {
          simpan_rumus();
        }
      });
    });

    function simpan_rumus() {
      var id_rumus = document.getElementsByName("id_rumus")[0].value
      var id_jenis_bangunan = document.getElementsByName("id_jenis_bangunan")[0].value
      var id_jenis_rumus = document.getElementsByName("id_jenis_rumus")[0].value
      var rumus = document.getElementsByName("rumus")[0].value

      swal.fire({
        title: "Apakah anda yakin?",
        text: "Apakah data ini akan disimpan!",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-primary",
        confirmButtonText: "Ya, Simpan",
        buttonsStyling: true
      }).then((result) => {
        if (result.value) {
          $.ajax({
            type: "POST",
            url: "<?php echo site_url() ?>master/rumus/proses",
            data: {
              "id_rumus": id_rumus,
              "id_jenis_bangunan" : id_jenis_bangunan,
              "id_jenis_rumus" : id_jenis_rumus,
              "rumus" : rumus
            },
            dataType: "json",
            beforeSend: function(request) {
              $.blockUI({
                message: '<h2><img src="<?php echo base_url('assets/lib/block-ui/images/gif.gif'); ?>" /> Please wait...</h2>',
                css: {
                  border: 'none',
                  padding: '15px',
                  '-webkit-border-radius': '10px',
                  '-moz-border-radius': '10px',
                  opacity: .9
                }
              });
            },
            success: function(response) {
              $.unblockUI()
              if (response.status == 1) {
                swal.fire({
                  title: "Berhasil!",
                  text: response.pesan,
                  type: "info"
                }).then(function() {
                  window.location.href="<?php echo site_url() ?>master/rumus";
                });
              } else {
                swal.fire({
                  title: "Gagal!",
                  text: response.pesan,
                  type: "warning"
                }, function() {
                  swal.fire.close();
                });
              }
            },
            failure: function(response) {
              swal(
                "Internal Error",
                "Oops, proses data gagal",
                "error"
              )
            }
          });
        }
      })
    }
  </script>