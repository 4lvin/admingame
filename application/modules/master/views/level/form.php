<div class="content-inner">
  <div class="col-lg-12">
    <div class="card">
      <div class="card-header d-flex align-items-center">
        <h3 class="h4"><?php echo isset($page_title) ? $page_title : 'Untitle'; ?></h3>
      </div>
      <div class="card-body">
        <form class="form-horizontal" action="<?php echo site_url('master/level/proses'); ?>" method="POST" id="form_level">
          <?php if (!empty($data))
            foreach ($data as $row)
              ?>
          <input name="id_level" type="hidden" value="<?php if (!empty($data)) echo $row->id_level ?>">
          <div class="form-group row">
            <label class="col-sm-3 form-control-label">Nama level<sup> *</sup></label>
            <div class="col-sm-9">
              <input name="nama_level" type="text" placeholder="Nama level" class="form-control form-control-success" value="<?php if (!empty($data)) echo $row->nama_level ?>">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-sm-3 form-control-label">Tampilkan Rumus<sup> *</sup></label>
            <div class="col-sm-9">
              <SELECT class="form-control show-tick" name="show_rumus">
                <?php foreach ($option_show_rumus as $option_rumus) {
                  if ($row->show_rumus != $option_rumus->value_setting) {
                    ?>
                    <option value="<?php echo $option_rumus->value_setting; ?>"><?php echo $option_rumus->nama_setting; ?></option>
                  <?php } else {
                  ?>
                    <option selected="selected" value="<?php echo $option_rumus->value_setting; ?>"><?php echo $option_rumus->nama_setting; ?></option>
                  <?php }
              } ?>
              </SELECT>
            </div>
          </div>
          <div class="form-group row">
            <div class="col-sm-9 offset-sm-3">
              <button type="submit" value="simpan" class="btn btn-primary" data-toggle="tooltip" title="Simpan">Simpan</button>
              <a href="<?php echo site_url('master/level'); ?>" class="btn btn-secondary" data-toggle="tooltip" title="kembali">Kembali</i></a>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
  <script>
    $(document).ready(function() {

      $("#form_level").validate({
        // Specify validation rules
        rules: {
          // The key name on the left side is the name attribute
          nama_level: "required",
          show_rumus: "required"

        },
        // Specify validation error messages
        messages: {
          nama_level: "Level harus diisi",
          show_rumus: "pilihan rumus ditampilkaan harus diisi"
        },
        // Make sure the form is submitted to the destination defined
        // in the "action" attribute of the form when valid
        submitHandler: function(form) {
          simpan_level();
        }
      });
    });

    function simpan_level() {
      var id_level = document.getElementsByName("id_level")[0].value
      var nama_level = document.getElementsByName("nama_level")[0].value
      var show_rumus = document.getElementsByName("show_rumus")[0].value

      swal.fire({
        title: "Apakah anda yakin?",
        text: "Apakah data ini akan disimpan!",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-primary",
        confirmButtonText: "Ya, Simpan",
        buttonsStyling: true
      }).then((result) => {
        if (result.value) {
          $.ajax({
            type: "POST",
            url: "<?php echo site_url() ?>master/level/proses",
            data: {
              "id_level": id_level,
              "nama_level" : nama_level,
              "show_rumus" : show_rumus
            },
            dataType: "json",
            beforeSend: function(request) {
              $.blockUI({
                message: '<h2><img src="<?php echo base_url('assets/lib/block-ui/images/gif.gif'); ?>" /> Please wait...</h2>',
                css: {
                  border: 'none',
                  padding: '15px',
                  '-webkit-border-radius': '10px',
                  '-moz-border-radius': '10px',
                  opacity: .9
                }
              });
            },
            success: function(response) {
              $.unblockUI()
              if (response.status == 1) {
                swal.fire({
                  title: "Berhasil!",
                  text: response.pesan,
                  type: "info"
                }).then(function() {
                  window.location.href="<?php echo site_url() ?>master/level";
                });
              } else {
                swal.fire({
                  title: "Gagal!",
                  text: response.pesan,
                  type: "warning"
                }, function() {
                  swal.fire.close();
                });
              }
            },
            failure: function(response) {
              swal(
                "Internal Error",
                "Oops, proses data gagal",
                "error"
              )
            }
          });
        }
      })
    }
  </script>