<div class="content-inner">
  <div class="col-lg-12">
    <div class="card">
      <div class="card-close">
        <a href="<?php echo site_url('master/level/form') ?>" class="btn btn-sm btn-warning">
          <i class="fas fa-plus-square"></i>Tambah data</a>
      </div>
      <div class="card-header d-flex align-items-center">
        <h3 class="h4"><?php echo isset($page_title) ? $page_title : 'Untitle'; ?></h3>
      </div>
      <div class="card-body">
        <table class="table table-bordered table-hover" id="table_level">
          <thead>
            <th class="text-center" width="30px">No</th>
            <th class="text-center">Nama level</th>
            <th class="text-center">Show rumus</th>
            <th class="text-center" width="70px">Aksi</th>
          </thead>
          <tbody>
            <?php
            $no = 0;
            if (empty($data)) {
              echo "<tr><td colspan = '10'><strong>Tidak Ada Data !</strong></td></tr>";
            } else {
              foreach ($data as $key) {
                $no++; ?>
                <tr>
                  <td><?php echo $no ?></td>
                  <td><?php echo $key->nama_level; ?></td>
                  <td><?php echo $key->nama_setting; ?></td>
                  <td>
                    <a href="<?php echo base_url('master/level/form') . '/' . $key->id_level; ?>" class="btn btn-sm btn-primary" data-toggle="tooltip" title="Edit"><i class="fa fas fa-edit" aria-hidden="true"></i></a>
                    <a onclick="delete_level(<?php echo $key->id_level; ?>)" class="btn btn-sm btn-danger" data-toggle="tooltip" title="Hapus" id="sa-params"> <i class="far fa-trash-alt" aria-hidden="true"></i></a>
                  </td>
                </tr>
              <?php
            }
          }
          ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>

  <script>
    $('#table_level').dataTable({
      searching: true,
      paging: true,
      responsive: true,
      pageLength: 5,
      "lengthMenu": [
        [5, 10, 25, 50],
        [5, 10, 25, 50]
      ]
    });

    function delete_level(id_level) {
      swal.fire({
        title: "Apakah anda yakin?",
        text: "Apakah data ini akan dihapus!",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-primary",
        confirmButtonText: "Ya, Hapus",
        buttonsStyling: true
      }).then((result) => {
        if (result.value) {
          $.ajax({
            type: "POST",
            url: "<?php echo site_url() ?>master/level/hapus",
            data: {
              "id_level": id_level
            },
            dataType: "json",
            beforeSend: function(request) {
              $.blockUI({
                message: '<h2><img src="<?php echo base_url('assets/lib/block-ui/images/gif.gif'); ?>" /> Please wait...</h2>',
                css: {
                  border: 'none',
                  padding: '15px',
                  '-webkit-border-radius': '10px',
                  '-moz-border-radius': '10px',
                  opacity: .9
                }
              });
            },
            success: function(response) {
              $.unblockUI()
              if (response.status == 1) {
                swal.fire({
                  title: "Berhasil!",
                  text: response.pesan,
                  type: "info"
                }).then(function() {
                  window.location.reload(true);
                });
              } else {
                swal.fire({
                  title: "Gagal!",
                  text: response.pesan,
                  type: "warning"
                }, function() {
                  swal.fire.close();
                });
              }
            },
            failure: function(response) {
              swal(
                "Internal Error",
                "Oops, proses data gagal",
                "error"
              )
            }
          });
        }
      })
    }
  </script>