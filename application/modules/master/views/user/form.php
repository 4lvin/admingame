<div class="content-inner">
  <div class="col-lg-12">
    <div class="card">
      <div class="card-header d-flex align-items-center">
        <h3 class="h4"><?php echo isset($page_title) ? $page_title : 'Untitle'; ?></h3>
      </div>
      <div class="card-body">
        <form class="form-horizontal" action="<?php echo site_url('master/user/proses'); ?>" method="POST" id="form_user">
          <?php if (!empty($data))
            foreach ($data as $row)
              ?>
          <input name="id_user" id= "id_user" type="hidden" value="<?php if (!empty($data)) echo $row->id_user ?>">
          <div class="form-group row">
            <label class="col-sm-3 form-control-label">nama user</label>
            <div class="col-sm-9">
              <input name="nama_user" type="text" placeholder="nama user" class="form-control form-control-success"
              value="<?php if (!empty($data)) echo $row->nama_user ?>">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-sm-3 form-control-label">username</label>
            <div class="col-sm-9">
              <input name="username" type="text" placeholder="username" class="form-control form-control-success"
              value="<?php if (!empty($data)) echo $row->username ?>">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-sm-3 form-control-label">password lama </label>
            <div class="col-sm-9">
              <input name="password_lama" type="text" placeholder="password lama" class="form-control form-control-success">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-sm-3 form-control-label">password baru </label>
            <div class="col-sm-9">
              <input name="password_baru" type="text" placeholder="password baru" class="form-control form-control-success">
            </div>
          </div>
          <div class="form-group row">
            <div class="col-sm-9 offset-sm-3">
              <button type="submit" value="simpan" class="btn btn-primary" data-toggle="tooltip" title="Simpan">Simpan</button>
              <a href="<?php echo site_url('master/user'); ?>" class="btn btn-secondary" data-toggle="tooltip" title="kembali">Kembali</i></a>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
  <script>
    $(document).ready(function() {

      $("#form_user").validate({
        // Specify validation rules
        rules: {
          // The key name on the left side is the name attribute
          nama_user: "required"

        },
        // Specify validation error messages
        messages: {
          nama_user: "nama user harus diisi"
        },
        // Make sure the form is submitted to the destination defined
        // in the "action" attribute of the form when valid
        submitHandler: function(form) {
          simpan_user();
        }
      });
    });

    function simpan_user() {
      var id_user = document.getElementsByName("id_user")[0].value
      var nama_user = document.getElementsByName("nama_user")[0].value

      swal.fire({
        title: "Apakah anda yakin?",
        text: "Apakah data ini akan disimpan!",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-primary",
        confirmButtonText: "Ya, Simpan",
        buttonsStyling: true
      }).then((result) => {
        if (result.value) {
          $.ajax({
            type: "POST",
            url: "<?php echo site_url() ?>master/user/proses",
            data: {
              "id_user": id_user,
              "nama_user" : nama_user
            },
            dataType: "json",
            beforeSend: function(request) {
              $.blockUI({
                message: '<h2><img src="<?php echo base_url('assets/lib/block-ui/images/gif.gif'); ?>" /> Please wait...</h2>',
                css: {
                  border: 'none',
                  padding: '15px',
                  '-webkit-border-radius': '10px',
                  '-moz-border-radius': '10px',
                  opacity: .9
                }
              });
            },
            success: function(response) {
              $.unblockUI()
              if (response.status == 1) {
                swal.fire({
                  title: "Berhasil!",
                  text: response.pesan,
                  type: "info"
                }).then(function() {
                  window.location.href="<?php echo site_url() ?>master/user";
                });
              } else {
                swal.fire({
                  title: "Gagal!",
                  text: response.pesan,
                  type: "warning"
                }, function() {
                  swal.fire.close();
                });
              }
            },
            failure: function(response) {
              swal(
                "Internal Error",
                "Oops, proses data gagal",
                "error"
              )
            }
          });
        }
      })
    }
  </script>