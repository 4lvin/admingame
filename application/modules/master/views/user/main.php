<div class="content-inner">
  <div class="col-lg-12">
    <div class="card">
      <div class="card-close">
      </div>
      <div class="card-header d-flex align-items-center">
        <h3 class="h4"><?php echo isset($page_title) ? $page_title : 'Untitle'; ?></h3>
      </div>
      <div class="card-body">
        <table class="table table-bordered table-hover" id="table_user">
          <thead>
            <th class="text-center" width="30px">No</th>
            <th class="text-center">Nama User</th>
            <th class="text-center" width="70px">Aksi</th>
          </thead>
          <tbody>
            <?php
            $no = 0;
            if (empty($data)) {
              echo "<tr><td colspan = '10'><strong>Tidak Ada Data !</strong></td></tr>";
            } else {
              foreach ($data as $key) {
                $no++; ?>
                <tr>
                  <td><?php echo $no ?></td>
                  <td><?php echo $key->nama_user; ?></td>
                  <td align="center">
                    <a href="<?php echo base_url('master/user/form') . '/' . $key->id_user; ?>" class="btn btn-sm btn-primary" data-toggle="tooltip" title="Edit"><i class="fa fas fa-edit" aria-hidden="true"></i></a>
                    </td>
                </tr>
              <?php
            }
          }
          ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>

  <script>
    $('#table_user').dataTable({
      searching: true,
      paging: true,
      responsive: true,
      pageLength: 5,
      "lengthMenu": [
        [5, 10, 25, 50],
        [5, 10, 25, 50]
      ]
    });
  </script>