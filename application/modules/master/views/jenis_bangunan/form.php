<div class="content-inner">
  <div class="col-lg-12">
    <div class="card">
      <div class="card-header d-flex align-items-center">
        <h3 class="h4"><?php echo isset($page_title) ? $page_title : 'Untitle'; ?></h3>
      </div>
      <div class="card-body">
        <form class="form-horizontal" action="<?php echo site_url('master/jenis_bangunan/proses'); ?>" method="POST" id="form_jenis_bangunan" enctype="multipart/form-data">
          <?php if (!empty($data))
            foreach ($data as $row)
              ?>
          <input name="id_jenis_bangunan" type="hidden" value="<?php if (!empty($data)) echo $row->id_jenis_bangunan ?>">
          <div class=" form-group row">
            <label class="col-sm-3 form-control-label">Jenis Bangunan<sup> *</sup></label>
            <div class="col-sm-9">
              <input name="nama_jenis_bangunan" type="text" placeholder="Jenis Bangunan" class="form-control form-control-success" value="<?php if (!empty($data)) echo $row->nama_jenis_bangunan ?>">
            </div>
          </div>
          <?php if (!empty($data)) { ?>
            <div class=" form-group row">
              <label class="col-sm-3 form-control-label">Gambar Jenis Bangunan<sup> *</sup></label>
              <div class="col-sm-9">
                <img src="<?php if ($row->url != "") {
                            echo base_url('uploads/'.$row->file_name);
                          } else {
                            echo base_url('uploads/default/default.jpg');
                          } ?>" alt="<?php echo $row->nama_jenis_bangunan; ?>" width="300" height="300" />
              </div>
            </div>
          <?php } ?>
          <div class=" form-group row">
            <label class="col-sm-3 form-control-label">Upload Gambar</label>
            <div class="col-sm-9">
              <input type="file" name="filefoto" id="image-upload" />
            </div>
          </div>
          <div class="form-group row">
            <div class="col-sm-9 offset-sm-3">
              <button type="submit" value="simpan" class="btn btn-primary" data-toggle="tooltip" title="Simpan">Simpan</button>
              <a href="<?php echo site_url('master/jenis_bangunan'); ?>" class="btn btn-secondary" data-toggle="tooltip" title="kembali">Kembali</i></a>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>

  <script>
    $('#form_jenis_bangunan').submit(function(evt) {
        evt.preventDefault();
        var formData = new FormData(this);

        swal.fire({
        title: "Apakah anda yakin?",
        text: "Apakah data ini akan disimpan!",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-primary",
        confirmButtonText: "Ya, Simpan",
        buttonsStyling: true
      }).then((result) => {
        if (result.value) {
          $.ajax({
            type: 'POST',
            url: $(this).attr('action'),
            data:formData,
            cache:false,
            contentType: false,
            processData: false,
            success: function(response) {
              var pesan = JSON.parse(response);
                if (pesan.status == 1) {
                  swal.fire({
                    title: "Berhasil!",
                    text: pesan.pesan,
                    type: "info"
                  }).then(function() {
                    window.location.href = "<?php echo site_url() ?>master/jenis_bangunan";
                  });
                } else {
                  swal.fire({
                    title: "Gagal!",
                    text: pesan.pesan,
                    type: "warning"
                  }, function() {
                    swal.fire.close();
                  });
                }
            },
            error: function(response) {
              swal(
                "Internal Error",
                "Oops, proses data gagal",
                "error"
              )
            }                        
          });
        }
      })
                
    });
  </script>