<div class="content-inner">
  <div class="col-lg-12">
    <div class="card">
      <div class="card-header d-flex align-items-center">
        <h3 class="h4"><?php echo isset($page_title) ? $page_title : 'Untitle'; ?></h3>
      </div>
      <div class="card-body">
        <form class="form-horizontal" action="<?php echo site_url('master/pertanyaan/proses'); ?>" method="POST" id="form_pertanyaan">
          <?php if (!empty($data))
            foreach ($data as $row)
              ?>
          <input name="id_pertanyaan" type="hidden" value="<?php if (!empty($data)) echo $row->id_pertanyaan ?>">
          <div class="form-group row">
            <label class="col-sm-3 form-control-label">Judul</label>
            <div class="col-sm-9">
              <input name="judul" type="text" placeholder="Nama rumus" class="form-control form-control-success" value="<?php if (!empty($data)) echo $row->judul ?>">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-sm-3 form-control-label">Pertanyaan</label>
            <div class="col-sm-9">
              <textarea name="pertanyaan" rows="4" class="form-control no-resize" placeholder="pertanyaan"><?php if (!empty($data)) echo $row->pertanyaan ?></textarea>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-sm-3 form-control-label">Pilihan A</label>
            <div class="col-sm-9">
              <input name="pilihan_a" type="text" placeholder="Pilihan A" class="form-control form-control-success" value="<?php if (!empty($data)) echo $row->pilihan_a ?>">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-sm-3 form-control-label">Pilihan B</label>
            <div class="col-sm-9">
              <input name="pilihan_b" type="text" placeholder="Pilihan B" class="form-control form-control-success" value="<?php if (!empty($data)) echo $row->pilihan_b ?>">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-sm-3 form-control-label">Pilihan C</label>
            <div class="col-sm-9">
              <input name="pilihan_c" type="text" placeholder="Pilihan C" class="form-control form-control-success" value="<?php if (!empty($data)) echo $row->pilihan_c ?>">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-sm-3 form-control-label">Pilihan D</label>
            <div class="col-sm-9">
              <input name="pilihan_d" type="text" placeholder="Pilihan D" class="form-control form-control-success" value="<?php if (!empty($data)) echo $row->pilihan_d ?>">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-sm-3 form-control-label">Kunci Jawaban</label>
            <div class="col-sm-9">
              <input name="kunci_jawaban" type="text" placeholder="Kunci jawaban" class="form-control form-control-success" value="<?php if (!empty($data)) echo $row->kunci_jawaban ?>">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-sm-3 form-control-label">Jenis Bangunan<sup> *</sup></label>
            <div class="col-sm-9">
              <SELECT class="form-control show-tick" name="id_jenis_bangunan">
                <?php foreach ($option_jenis_bangunan as $option_bangunan_jenis) {
                  if ($row->id_jenis_bangunan != $option_bangunan_jenis->id_jenis_bangunan) {
                    ?>
                    <option value="<?php echo $option_bangunan_jenis->id_jenis_bangunan; ?>"><?php echo $option_bangunan_jenis->nama_jenis_bangunan; ?></option>
                  <?php } else {
                  ?>
                    <option selected="selected" value="<?php echo $option_bangunan_jenis->id_jenis_bangunan; ?>"><?php echo $option_bangunan_jenis->nama_jenis_bangunan; ?></option>
                  <?php }
              } ?>
              </SELECT>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-sm-3 form-control-label">Jenis Rumus<sup> *</sup></label>
            <div class="col-sm-9">
              <SELECT class="form-control show-tick" name="id_jenis_rumus">
                <?php foreach ($option_jenis_rumus as $option_rumus_jenis) {
                  if ($row->id_jenis_rumus != $option_rumus_jenis->id_jenis_rumus) {
                    ?>
                    <option value="<?php echo $option_rumus_jenis->id_jenis_rumus; ?>"><?php echo $option_rumus_jenis->nama_jenis_rumus; ?></option>
                  <?php } else {
                  ?>
                    <option selected="selected" value="<?php echo $option_rumus_jenis->id_jenis_rumus; ?>"><?php echo $option_rumus_jenis->nama_jenis_rumus; ?></option>
                  <?php }
              } ?>
              </SELECT>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-sm-3 form-control-label">Level<sup> *</sup></label>
            <div class="col-sm-9">
              <SELECT class="form-control show-tick" name="id_level">
                <?php foreach ($option_level as $option_level) {
                  if ($row->id_level != $option_level->id_level) {
                    ?>
                    <option value="<?php echo $option_level->id_level; ?>"><?php echo $option_level->nama_level; ?></option>
                  <?php } else {
                  ?>
                    <option selected="selected" value="<?php echo $option_level->id_level; ?>"><?php echo $option_level->nama_level; ?></option>
                  <?php }
              } ?>
              </SELECT>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-sm-3 form-control-label">Status Aktif<sup> *</sup></label>
            <div class="col-sm-9">
              <SELECT class="form-control show-tick" name="is_aktif">
                <?php foreach ($option_status_soal as $option_soal_status) {
                  if ($row->is_aktif != $option_soal_status->value_setting) {
                    ?>
                    <option value="<?php echo $option_soal_status->value_setting; ?>"><?php echo $option_soal_status->nama_setting; ?></option>
                  <?php } else {
                  ?>
                    <option selected="selected" value="<?php echo $option_soal_status->value_setting; ?>"><?php echo $option_soal_status->nama_setting; ?></option>
                  <?php }
              } ?>
              </SELECT>
            </div>
          </div>
          <div class="form-group row">
            <div class="col-sm-9 offset-sm-3">
              <button type="submit" value="simpan" class="btn btn-primary" data-toggle="tooltip" title="Simpan">Simpan</button>
              <a href="<?php echo site_url('master/pertanyaan'); ?>" class="btn btn-secondary" data-toggle="tooltip" title="kembali">Kembali</i></a>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>

  <script>
    $(document).ready(function() {

      $("#form_pertanyaan").validate({
        // Specify validation rules
        rules: {
          // The key name on the left side is the name attribute
          judul: "required",
          pertanyaan: "required",
          pilihan_a: "required",
          pilihan_b: "required",
          pilihan_c: "required",
          pilihan_d: "required",
          kunci_jawaban: "required",
          id_jenis_bangunan: "required",
          id_jenis_rumus: "required",
          id_level: "required",
          is_aktif: "required"

        },
        // Specify validation error messages
        messages: {
          judul: "judul harus diisi",
          pertanyaan: "pertanyaan harus diisi",
          pilihan_a: "pilihan a harus diisi",
          pilihan_b: "pilihan b harus diisi",
          pilihan_c: "pilihan c harus diisi",
          pilihan_d: "pilihan d harus diisi",
          kunci_jawaban: "required",
          id_jenis_bangunan: "pilihan jenis bangunan harus diisi",
          id_jenis_rumus: "pilihan jenis rumus harus diisi",
          id_level: "pilihan level harus diisi",
          is_aktif: "pilihan status aktif harus diisi"
        },
        // Make sure the form is submitted to the destination defined
        // in the "action" attribute of the form when valid
        submitHandler: function(form) {
         simpan_pertanyaan();
        }
      });
    });

    function simpan_pertanyaan() {
      var id_pertanyaan = document.getElementsByName("id_pertanyaan")[0].value
      var judul = document.getElementsByName("judul")[0].value
      var pertanyaan = document.getElementsByName("pertanyaan")[0].value
      var pilihan_a = document.getElementsByName("pilihan_a")[0].value
      var pilihan_b = document.getElementsByName("pilihan_b")[0].value
      var pilihan_c = document.getElementsByName("pilihan_c")[0].value
      var pilihan_d = document.getElementsByName("pilihan_d")[0].value
      var kunci_jawaban = document.getElementsByName("kunci_jawaban")[0].value
      var id_jenis_bangunan = document.getElementsByName("id_jenis_bangunan")[0].value
      var id_jenis_rumus = document.getElementsByName("id_jenis_rumus")[0].value
      var id_level = document.getElementsByName("id_level")[0].value
      var is_aktif = document.getElementsByName("is_aktif")[0].value

      swal.fire({
        title: "Apakah anda yakin?",
        text: "Apakah data ini akan disimpan!",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-primary",
        confirmButtonText: "Ya, Simpan",
        buttonsStyling: true
      }).then((result) => {
        if (result.value) {
          $.ajax({
            type: "POST",
            url: "<?php echo site_url() ?>master/pertanyaan/proses",
            data: {
              "id_pertanyaan": id_pertanyaan,
              "judul": judul,
              "pertanyaan": pertanyaan,
              "pilihan_a": pilihan_a,
              "pilihan_b": pilihan_b,
              "pilihan_c": pilihan_c,
              "pilihan_d": pilihan_d,
              "kunci_jawaban": kunci_jawaban,
              "id_jenis_bangunan": id_jenis_bangunan,
              "id_jenis_rumus": id_jenis_rumus,
              "id_level": id_level,
              "is_aktif" : is_aktif
            },
            dataType: "json",
            beforeSend: function(request) {
              $.blockUI({
                message: '<h2><img src="<?php echo base_url('assets/lib/block-ui/images/gif.gif'); ?>" /> Please wait...</h2>',
                css: {
                  border: 'none',
                  padding: '15px',
                  '-webkit-border-radius': '10px',
                  '-moz-border-radius': '10px',
                  opacity: .9
                }
              });
            },
            success: function(response) {
              $.unblockUI()
              if (response.status == 1) {
                swal.fire({
                  title: "Berhasil!",
                  text: response.pesan,
                  type: "info"
                }).then(function() {
                  window.location.href="<?php echo site_url() ?>master/pertanyaan";
                });
              } else {
                swal.fire({
                  title: "Gagal!",
                  text: response.pesan,
                  type: "warning"
                }, function() {
                  swal.fire.close();
                });
              }
            },
            failure: function(response) {
              swal(
                "Internal Error",
                "Oops, proses data gagal",
                "error"
              )
            }
          });
        }
      })
    }
  </script>