<?php
if (!defined("BASEPATH"))
	exit("No direct script access allowed");

class Login extends MX_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->library('form_validation');
		$this->load->model('Login_model', 'admin');
	}

	function index()
	{
		$this->load->view('template/header');
		$this->load->view('template/js');
		$this->load->view('login');
		$this->load->view('template/footer');
	}

	function proses()
	{
		// https://www.rubypedia.com/blog/tutorial-cara-membuat-login-session-dengan-codeigniter-dan-bootstrap/
		if ($this->admin->logged_id()) {
			//jika memang session sudah terdaftar, maka redirect ke halaman dahsboard
			redirect("dashboard");
		} else {

			//jika session belum terdaftar

			//set form validation
			$this->form_validation->set_rules('loginUsername', 'Username', 'required');
			$this->form_validation->set_rules('loginPassword', 'Password', 'required');

			//set message form validation
			$this->form_validation->set_message('required', '<div class="alert alert-danger" style="margin-top: 3px">
                    <div class="header"><b><i class="fa fa-exclamation-circle"></i> {field}</b> harus diisi</div></div>');

			//cek validasi
			if ($this->form_validation->run() == TRUE) {

				//get data dari FORM
				$username = $this->input->post("loginUsername", TRUE);
				$password = MD5($this->input->post('loginPassword', TRUE));

				//checking data via model
				$checking = $this->admin->check_login(array('username' => $username), array('password' => $password));

				//jika ditemukan, maka create session
				if ($checking != FALSE) {
					foreach ($checking as $apps) {

						$session_data = array(
							'user_id'   => $apps->id_user,
							'username' => $apps->username,
							'password' => $apps->password,
						);
						//set session userdata
						$this->session->set_userdata($session_data);

						redirect('dashboard/');
					}
				} else {

					$data['error'] = '<div class="alert alert-danger" style="margin-top: 3px">
                        <div class="header"><b><i class="fa fa-exclamation-circle"></i> ERROR</b> username atau password salah!</div></div>';
					$this->load->view('template/header');
					$this->load->view('template/js');
					$this->load->view('login', $data);
					$this->load->view('template/footer');
				}
			} else {
				redirect(base_url("login"));
			}
		}
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect('login');
	}
}
