<div class="content-inner">
  <div class="col-lg-12">
                  <div class="card">
                    <div class="card-close">
                    <a href="#" data-toggle="modal" data-target="#tambahJamaahModal" class="btn btn-sm btn-warning">
                    Tambah User <i class="fas fa-plus-square"></i></a>
                    </div>
                    <div class="card-header d-flex align-items-center">
                      <h3 class="h4">Daftar User</h3>
                    </div>
                    <div class="card-body">
                      <table class="table table-bordered table-hover" id="table_user">
                        <thead>
                            <th class="text-center">No</th>
                            <th class="text-center">Nama lengkap</th>
                            <th class="text-center">Tanggal lahir</th>
                            <th class="text-center">Alamat</th>
                            <th class="text-center">No. Hp</th>
                            <th class="text-center">-</th>
                        </thead>
                         <tbody>
                        <?php
                          $no = 0;
                          if (empty($data)) {
                                echo "<tr><td colspan = '10'><strong>Tidak Ada Data !</strong></td></tr>";
                          }else {
                              foreach ($data as $key) {
                              $no++; ?>
                          <tr>
                          <td><?php echo $no?></td>
                          <td><?php echo $key->nama;?></td>
                          <td><?php echo $key->tanggal_lahir;?></td>
                          <td><?php echo $key->alamat;?></td>
                          <td><?php echo $key->no_hp;?></td>
                          <td><center>
                          <a href="<?php echo base_url('user/edit_user') . '/' . $key->id_user; ?>"
                          class="btn btn-sm btn-primary" data-toggle="tooltip"
                          title="Edit"> <i class="fas fa-edit" aria-hidden="true"></i></a>
                          <a onclick="delete_user(<?php echo $key->id_user; ?>)"
                          class="btn btn-sm btn-danger" data-toggle="tooltip"
                          title="Hapus" id="sa-params"> <i class="far fa-trash-alt" aria-hidden="true"></i></a>
                          </td></center>
                          </tr>
                          <?php
                              }
                            }
                          ?>
                        </tbody>
                      </table>
                    </div>
                  </div>
</div>

<script>
    $('#table_user').dataTable({
        searching: true,
        paging: true,
        responsive: true
    });

 $(document).ready(function () {
    $("#btn_edit").click(function() {
      var id = document.getElementById("btn_edit").value;
      alert(id);

    })

  })

  function delete_user(id_user){
    swal({
                title: "Apakah anda yakin?",
                text: "Apakah pertanyaan ini akan dihapus!",
                type: "info",
                showCancelButton: true,
                confirmButtonClass: "btn-primary",
                confirmButtonText: "Ya, Hapus",
                closeOnConfirm: true
            },
            function (isConfirm) {
                if (isConfirm) {
                    var url = "<?php echo base_url() ?>user/hapus_user";

                    $.ajax({
                        type: "POST",
                        url: url,
                        data: {"id_user": id_user},
                        dataType: "json",
                        beforeSend:function(request) {
                            $.blockUI({ message: '<h2><img src="<?php echo base_url('asset/lib/block-ui/images/gif.gif'); ?>" /> Please wait...</h2>',
                                css: {
                                        border: 'none',
                                        padding: '15px',
                                        '-webkit-border-radius': '10px',
                                        '-moz-border-radius': '10px',
                                        opacity: .9
                                    }
                            });   
                        },
                        success: function (data) {
                            $.unblockUI()
                            if(data.status==1){
                                swal({
                                    title: "Berhasil!",
                                    text: data.pesan,
                                    type: "warning"
                                },function(){ 
                                        window.location.reload(true);
                                    }
                                );
                            }else{
                                swal({
                                    title: "Gagal!",
                                    text: data.pesan,
                                    type: "warning"
                                },function(){ 
                                       swal.close();
                                    }
                                );
                            }
                        }
                    });

                    
                }
            });
  }
</script>