<?php 
if (!defined("BASEPATH"))
    exit("No direct script access allowed");

class user extends MX_Controller{

	function __construct(){
		parent::__construct();	
		$this->load->helper('url');	
		$this->load->model('user_model', 'tbl_get');
	}

	function index(){
		$data['data'] = $this->tbl_get->get_user();
		$this->load->view('template/header');
		$this->load->view('template/js');
		$this->load->view('template/navbar');
		$this->load->view('template/sidebar');
		$this->load->view('main_user', $data);
		$this->load->view('template/footer');
	}

	function hapus_user(){
		$id_user = $this->input->post('id_user');
		if($id_user!=""){
			if ($this->tbl_get->delete_user($id_user)) {
				$d['status'] = 1;
				$d['pesan'] = "data berhasil dihapus.";
			}
			else{
				$d['status'] = 0;
				$d['pesan'] = "GAGAL! data tidak berhasil di hapus";
			}
		}
		echo json_encode($d);
	}

}
?>